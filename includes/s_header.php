<!DOCTYPE html>
<?php
session_start();
include("conf.php");
$user=$_SESSION['user_email'];
		  $get_name="select * from users where user_email='$user'";
		  $run_sql=mysqli_query($con,$get_name);
		  $row_name=mysqli_fetch_array($run_sql);
		  $user_nam=$row_name['user_name'];
		  $user_id=$row_name['user_id'];
?>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
     <script>
        tinymce.init({selector:'textarea'});
   </script>

    <title>SHU_Sellar Dashboard</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    
    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


  <body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				 <a class="navbar-brand" href="index.php"> <i class="glyphicon glyphicon-home"></i></a>
                         </div>
                         <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    
                <li>
                        <a href="all_pro.php">Browse Books</a>
                    </li>
                    <li>
                        <a href="faq.php">FAQ</a>
                    </li>
                    <li>
                        <a href="https://databaseandweb.wordpress.com/">Blog</a>
                    </li>
                    
                    <li>
                        <a href="contact_us.php">Contact</a>
                    </li>
                </ul>
                <div>
<ul class="nav navbar-right top-nav">
                               
                <li class="dropdown">
                    <?php 
                    if(isset($_SESSION['user_email'])){
                    echo  "<a href='' class='dropdown-toggle' data-toggle='dropdown'><i class='fa fa-user'></i>$user_nam" ;
                    }
                    else {
                    echo "<b>Welcome Guest!</b>";
                    }
                    ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="spass_change.php"><i class="fa fa-fw fa-gear"></i> Change Password</a>
                        </li>
                        <li>
                            <a href="edit_saccount.php"><i class="fa fa-fw fa-gear"></i> Edit Account</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>



                </div>
            </div>
            <!-- /.navbar-collapse -->
            <!-- Top Menu Items -->

                        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="sellar_dashboard.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="Add_product.php"><i class="fa fa-fw fa-bar-chart-o"></i> Add Book</a>
                    </li>
                    <li>
                        <a href="view_product.php"><i class="fa fa-fw fa-table"></i> Modify Books</a>
                    </li>
                    <li>
                        <a href="view_clients.php"><i class="fa fa-fw fa-edit"></i>View Orders </a>
                    </li>
                    <li>
                        <a href="seller_profile.php"><i class="fa fa-fw fa-wrench"></i> Profile Information</a>
                    </li>
                                    </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        
</body>
</html>
	 