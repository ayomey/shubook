<!DOCTYPE html>
<?php
session_start();
include("includes/conf.php");
include("includes/Pro_cat.php");
?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SHU BOOK STORE| </title>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Abeezee:400|Open+Sans:400,600,700|Source+Sans+Pro:400,600">
	<link rel="stylesheet" type="text/css" href="FAQ/defaults.css">
	<link rel="stylesheet" type="text/css" href="FAQ/demo.css">

	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="FAQ/accordion.js"></script>
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">
	 <link href="css/search.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<link href="css/rating.css" rel="stylesheet">
	<link href="css/view.css" rel="stlesheet">
	<link href="css/star-rating.css" media="all" rel="stylesheet" type="text/css" />
     <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
     <script src="js/star-rating.js" type="text/javascript"></script>   
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    </head><!--/head-->

<body>
	<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					 <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="col-sm-8">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"> <i class="glyphicon glyphicon-home"></i></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <?php if(!isset($_SESSION['user_email'])){
                    	echo"<li>
                        <a href='registration.php'>Sign Up</a>
                    </li>";
                    }
                    ?>
     			<li>
     				   <?php
                          if(isset($_SESSION['user_email'])){
     				   $user_type = $_SESSION['user_email'];

     				   $sql= "select * from users where user_email= '$user_type'";
     				   $run_sql=mysqli_query($con,$sql);
		    $row_user=mysqli_fetch_array($run_sql);
		    $user_id=$row_user['user_id'];
            $user_typet = $row_user['user_type']; 
     				    if(!$user_typet ==1){
                    	echo"<li>
                        <a href='sellar_dashboard.php'>Sellar Dashboard</a>
                    </li>";
                    }}
                    ?>
     			<li>
                        <a href="all_pro.php">Browse Books</a>
                    </li>
                    <li>
                        <a href="faq.php">FAQ</a>
                    </li>
					<li>
                        <a href="https://databaseandweb.wordpress.com/">Blog</a>
                    </li>
					
                    <li>
                        <a href="contact_us.php">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
               </nav>
   				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle" style="margin-top:50px"><!--header-middle-->
			<div class="container">
				<div class="row">
				<div class="col-sm-4">
	            <form  class="search-form" method="GET" action="result.php" enctype="multipart/form-data">
                <div class="form-group has-feedback">
            		<label for="search" class="sr-only">Search</label>
            		<input type="text" class="form-control" name="search_key"  placeholder="search">
              		<span class="glyphicon glyphicon-search form-control-feedback"><input type="submit" name="search" style="visibility: hidden;"></span>
            	</div>
            </form>
           </div><!-- /input-group -->
		   <?php cart();?>
  					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
							
					<?php 
					if(isset($_SESSION['user_email'])){
						$user=$_SESSION['user_email'];
		        $get_name="select * from users where user_email='$user'";
		  $run_sql=mysqli_query($con,$get_name);
		  $row_name=mysqli_fetch_array($run_sql);
		  $user_nam=$row_name['user_name'];
		  $user_type=$row_name['user_type'];
		  if ($user_type==1)
		  {echo " <li><a href='my_account.php'><i class='fa fa-user'></i> <b> Welcome $user_nam </b>"; } else
			  {echo " <li><a href='my_account.php'><i class='fa fa-user'></i> <b> Welcome $user_nam </b>";}
	  		    										}
														
					if(!isset($_SESSION['user_email'])){
								echo " <li><a href='index.php'><i class='fa fa-user'></i><b>Welcome Guest!</b>";
					}
					
					
					
					?></a></li>
								
								<li><a href="cart.php"><i class="fa fa-shopping-cart"></i> <?php echo total_items();?></a></li>
								<li>
								<?php
								if(!isset($_SESSION['user_email'])){
									
							echo"	<a href='login.php'><i class='fa fa-lock'></i> Login</a></li>";
								}else{
									
								echo" <a href='logout.php'><i class='fa fa-unlock'></i> Logout</a></li>";	
								}
								
														?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<!--/header-bottom-->
	</header><!--/header-->
	
	
  <script src="js/rating.js"></script>
    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
