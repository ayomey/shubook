<!DOCTYPE html>
<?php
include("includes/c_header.php");
?>

<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SHU Books Website</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/full-slider.css" rel="stylesheet">
	  <link href="css/login.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
 <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"> <i class="glyphicon glyphicon-home"></i></a>
            </div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="registration.php">Register</a>
                    </li>
     			<li>
                        <a href="all_pro.php">Browse Books</a>
                    </li>
					<li>
                        <a href="#">Blog</a>
                    </li>
					
                    <li>
                        <a href="#">Contact Us</a>
                    </li>
                </ul>
            </div>
			 </nav>
			      <div class="container-fluid" style="margin-top:80px">
       <section class="container">
		<div class="container-page">				
							<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<strong> Sign in to continue</strong>
					</div>
					<div class="panel-body">
						<form role="form" method="POST">
							<fieldset>
								<div class="row">
									<div class="center-block">
										<img class="profile-img"
											src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt="">
									</div>
								</div>

								<div class="row">
									<div class="col-sm-12 col-md-10  col-md-offset-1 ">
									
									<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-user"></i>
												</span> 
												 <select class="form-control" name="type" autofocus>

            <option value="select">I am..</option>

            <option value="Seller">Buyer / Seller</option>

            <option value="Student">Buyer</option>

                       </select>
															</div>
										</div>
										
										
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-user"></i>
												</span> 
												<input class="form-control" placeholder="Email Address" name="loginname" type="text" autofocus required>
											</div>
										</div>
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-lock"></i>
												</span>
												<input class="form-control" placeholder="Password" name="password" type="password" required>
											</div>
										</div>
																					<div class="form-group">
											<input type="submit" name="login" class="btn btn-lg btn-primary btn-block" value="Sign in">
										</div>
									</div>
								</div>
							</fieldset>
						</form>
					</div>
					<div class="panel-footer ">
						Don't have an account! <a href="registration.php" onClick=""> Sign Up Here </a>
					</div>
                   </div>
				   </div>
				   <div class="col-md-6">
				   <div class="row text-center" style="margin-top:10px">
				<h4 class="dark-grey">Welcome to SHU Book Store</h4>
				<img src="img/login.png"style : "margin-left:130px">
				</div>
				</div>
				</section>
         		</div>
	</div>
	</body>
	</html>
	
	<?php
	if(isset($_POST['login']))
	{
		
		$user_email=$_POST['loginname'];
		$user_pass=$_POST['password'];
		// To protect MySQL injection
		$user_email = stripslashes($user_email);
       $user_pass = stripslashes($user_pass);
      $user_email = mysql_real_escape_string($user_email);
   $user_pass = mysql_real_escape_string($user_pass);
		$user_type=$_POST['type'];
		if($user_type=="Student"){
			
			$type=1;
		}else
		{
			$type=0;
		}
         
		$sel_user="select * from users where user_type='$type' AND user_pass='$user_pass' AND user_email='$user_email' ";
		$run_sql=mysqli_query($con,$sel_user);
        $check_user=mysqli_num_rows($run_sql);
						 if($check_user==0)
			 {
				 echo"<script>alert('Please check your email or password') </script>";
				 echo"<script>window.open('Login.php','_self')</script>";
				 exit();
			 }	
			 $ip=getIp();
			 $sel_cart="select * from cart where ip_add='$ip'";
			 $run_cart=mysqli_query($con,$sel_cart);
			 $check_cart=mysqli_num_rows($run_cart);
			  if ($check_user>0 AND $type==0){   
						   
						    $_SESSION['user_email']=$user_email;
			   echo"<Script>alert('Welcome to SHU Account')</script>";
			   echo"<script>window.open('sellar_dashboard.php','_self')</script>";
					   }	
			 
           if ($check_user>0 AND $type==1  AND $check_cart==0)
		   {
			   $_SESSION['user_email']=$user_email;
			   echo"<Script>alert('Welcome to SHU Account')</script>";
			   echo"<script>window.open('index.php','_self')</script>";
			   		   } else{
						   $_SESSION['user_email']=$user_email;
			   echo"<Script>alert('Welcome to SHU Account')</script>";
			   echo"<script>window.open('index.php','_self')</script>";
					   }
						   
						 		   
	}
	
	?>