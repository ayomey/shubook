<!DOCTYPE html>
<?php
include("includes/c_header.php");

?>

   <div class="container" style="margin-top:80px">
    <div class="row">
        <div class="col-sm-12 col-md-10 col-md-offset-1">
            <form method="post" action="" enctype="multipart/form-data">
            <table class="table table-hover" >
                <thead>
                    <tr>
                        <th>Product</th>
						
                        <th>Quantity</th>
						<th></th>
						<th></th>
                        <th class="text-center">Price</th>
						<th></th>
						<th></th>
						<th></th>
						
                        <th class="text-center">Remove</th>
                        <th> </th>
                    </tr>
                </thead>
				<tbody>
                <?php 
        $total = 0;
        
        global $con; 
        
        $ip = getIp(); 
        
        $sel_price = "select * from cart where ip_add='$ip'";
        
        $run_price = mysqli_query($con, $sel_price); 
        
        while($p_price=mysqli_fetch_array($run_price)){
            
            $pro_id = $p_price['p_id']; 
            
            $pro_price = "select * from products where product_id='$pro_id'";
            
            $run_pro_price = mysqli_query($con,$pro_price); 
            
            while ($pp_price = mysqli_fetch_array($run_pro_price)){
            
            $product_price = array($pp_price['product_price']);
            
            $product_title = $pp_price['product_title'];
            $product_author = $pp_price['product_autor'];
            
            $product_image = $pp_price['product_image']; 
            
            $single_price = $pp_price['product_price'];

            $pro_userid = $pp_price['user_id'];

            $pro_qsold = $pp_price['product_qsold'];
            $pro_stock = $pp_price['product_qty'];
            
            $values = array_sum($product_price); 
            
            $total += $values; 
                    
                    ?>
                
                    <tr>
                        <td class="col-sm-8 col-md-6">
                        <div class="media">
                           <img src="product _images/<?php echo $product_image;?>"> 
                            <div class="media-body">
                                <h4 class="media-heading"><?php echo $product_title; ?></h4>
                                                           </div>
                        </div></td>
						<?php 
						 global $con; 
        
                         $ip = getIp(); 
						 
                        if(isset($_POST['update_cart'])){
                        
                            $qty = $_POST['qty'];
							
							 $_SESSION['qty']=$qty;
							
                            $update_qty = "update cart set qty='$qty' where p_id='$pro_id' AND ip_add='$ip'";
                            
                            $run_qty = mysqli_query($con, $update_qty); 
                            
                           
							
							if ($_SESSION['qty']>=1)
							{
                            
                            $total = $total*$qty;
							} 
                        }
                        
                        
                        ?>
						
                        <td class="col-sm-1 col-md-1" style="text-align: center">
                        <input type="text" name="qty"  size="4" width="4px" value="">
                        </td>
                        
						<td></td>
						<td></td>
                        <td class="col-sm-1 col-md-1 text-center"><strong><?php echo "£" . $single_price; ?></strong></td>
						<td></td>
						<td></td>
						<td></td>
						
                        <td class="col-sm-1 col-md-1">
                        <input type="checkbox" name="remove[]" value="<?php echo $pro_id;?>">
                            <span class="glyphicon glyphicon-remove"></span> 
                        </button></td>
                    </tr>
                    <?php } } ?>
                    
                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td>   </td>
                        <td><h3>Total</h3></td>
                        <td class="text-right"><h3><strong><?php echo "£" . $total;?></strong></h3></td>
                    </tr>
                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td>   </td>
                        <td>
                        <button class="btn btn-default"type="submit" name="update_cart" value="Update Cart">
                            Update Cart
                        </button></td>
                        <td>
                        <button class="btn btn-default"type="submit" name="continue" value="Continue Shopping">
                            <span class="glyphicon glyphicon-shopping-cart"></span> Continue Shopping
                        </button></td>
                        <td>
						
                        <button type="submit" class="btn btn-success" name='checkout'>
                            <span class="glyphicon glyphicon-play"> Checkout</span>
                        </button></td>
						 </tr>
                </tbody>
            </table>
        </form>     </div>
    </div>
</div>
        
						<?php

        
    function updatecart(){
        
        global $con; 
        
        $ip = getIp();
        
        if(isset($_POST['remove'])){
        
            foreach($_POST['remove'] as $remove_id){
            
            $delete_product = "delete from cart where p_id='$remove_id' AND ip_add='$ip'";
            
            $run_delete = mysqli_query($con, $delete_product); 
            
            if($run_delete){
            
            echo "<script>window.open('cart.php','_self')</script>";
            
            }
            
            }
        
        }
        if(isset($_POST['continue'])){
        
        echo "<script>window.open('all_pro.php','_self')</script>";
        
        }
    
    }
    echo @$up_cart = updatecart();
    


if(isset($_POST['checkout']))
							  {
								  $ip = getIp(); 
        
        $sql = "select * from cart where ip_add='$ip'";
        
            $run_sql = mysqli_query($con, $sql); 
            
            $row = mysqli_fetch_array($run_sql); 
            
            $qty = $row['qty'];
			if($qty==0){
            
            $qty=1;
            }
            else {
            
            $qty=$qty;
            
            $total = $total*$qty;
            
            }
			$product_id=$row['p_id'];
			
			  if(!$_SESSION['user_email'])
			  {
				
				echo"
				<script>alert('Login or Register to checkout')</script>
		      <script>window.open('Login.php','_self')</script>";  
				  
				  
			  }else{
				  $user=$_SESSION['user_email'];
		    $get_user="select * from users where user_email='$user'";
		    $run_sql=mysqli_query($con,$get_user);
		    $row_user=mysqli_fetch_array($run_sql);
		    $user_id=$row_user['user_id'];
            $user_name = $row_user['user_name']; 
            $user_email = $row_user['user_email']; 
            
            
                     
                // inserting the order into table
                $insert_order = "insert into orders (p_id, u_id, s_id, qty,status) values ('$product_id','$user_id','$user_id','$qty','in Progress')";
                $run_order = mysqli_query($con, $insert_order); 
                //removing the products from cart
                $empty_cart = "delete from cart where p_id='$product_id'";
                $run_cart = mysqli_query($con, $empty_cart);


                // inserting into product the new sold item for best seller list 
                $get_quantity = "select product_qty, product_qsold from products where product_id= '$product_id'";
                $run_qty = mysqli_query($con,$get_quantity);
                $row_qty = mysqli_fetch_array($run_qty);
                $pro_qty = $row_qty['product_qty'];
                $pro_sqty = $row_qty['product_qsold'];
                $latest_qtysold = $pro_sqty + $qty;
                $update_bestseller = "update products set product_qsold = '$latest_qtysold' where product_id = '$product_id'";
                $run_bestseller = mysqli_query($con,$update_bestseller);
                 // inserting into product the new sold item for stock analysis 
                $latest_stock = $pro_qty - $qty;
                $update_stock = "update products set product_qty = '$latest_stock' where product_id = '$product_id'";
                $run_stock = mysqli_query($con,$update_stock);

                // email student after product bought

                          //get seller email
             $get_seller="select * from users where user_id='$pro_userid'";
            $run_sellersql=mysqli_query($con,$get_seller);
            $row_seller=mysqli_fetch_array($run_sellersql);
            $seller_id=$row_seller['user_id'];
            $seller_name = $row_seller['user_name'];
            $seller_email = $row_seller['user_email'];

           
require 'PHPMailer/PHPMailerAutoload.php';

$message = "<html><body>        
<table rules='all' style='border-color: #666;'' cellpadding='10'>
<tr style='background: #eee;'><td>Hello <strong>$user_name</strong><p> you have ordered some books from SHU BOOKStore, please find your order details below. Contact the seller to negotiate payment and delivery</p> </td></tr>
<tr><td><strong>Seller Email:</strong> $seller_email</td></tr>
<tr><td><strong>Book Title:</strong> $product_title By <strong>$product_author </td></tr>
<tr><td><strong>Price:</strong>£ $total </td></tr>
<tr><td><h3>You can also go to your account to view the order details <a href='http://localhost:8080/group_2/my_account.php?my_orders'><strong>My Account<strong></a></h3></td></tr>
<tr style='background: #eee;'><td><strong>Thank you for your order @ - SHU Store</strong>  </td></tr>
                        
                
                </table></body></html>";
                $smessage = "<html><body>        
<table rules='all' style='border-color: #666;'' cellpadding='10'>
<tr style='background: #eee;'><td>Hello <strong>$seller_name</strong><p> someone just ordered a book of yours from SHU BOOKStore, please find the order details below. Contact the Buyer to negotiate payment and delivery</p> </td></tr>
<tr><td><strong>Customer Email:</strong> $user_email</td></tr>
<tr><td><strong>Book Title:</strong> $product_title By <strong>$product_author </td></tr>
<tr><td><strong>Price:</strong>£ $total </td></tr>
<tr><td><h3>You can also go to your account to view the order details <a href='http://localhost:8080/group_2/seller_dashboard.php'><strong>My Account<strong></a></h3></td></tr>
<tr style='background: #eee;'><td><strong>Thank you for selling @ - SHU Store</strong>  </td></tr>
                        
                
                </table></body></html>";

$mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'databaseandw@gmail.com';                 // SMTP username
$mail->Password = 'PasswoRD';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to

$mail->From = 'databaseandw@gmail.com';
$mail->FromName = 'SHU BookStore';
$mail->addAddress($user_email, $user_email);     // Add a recipient
//$mail->addAddress('ellen@example.com');               // Name is optional
$mail->addReplyTo($seller_email, $seller_name);
//$mail->addCC('cc@example.com');
//$mail->addBCC('bcc@example.com');

//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = 'SHU BookStore Order Confirmation';
$mail->Body    = $message;
$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

if(!$mail->send()) {

} else {

}
// seller email
$mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'databaseandw@gmail.com';                 // SMTP username
$mail->Password = 'PasswoRD';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to

$mail->From = 'databaseandw@gmail.com';
$mail->FromName = 'SHU BookStore';
$mail->addAddress($seller_email, $seller_email);     // Add a recipient
//$mail->addAddress('ellen@example.com');               // Name is optional
$mail->addReplyTo($user_email, $user_name);
//$mail->addCC('cc@example.com');
//$mail->addBCC('bcc@example.com');

//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = 'SHU BookStore Book Sold';
$mail->Body    = $smessage;
$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

if(!$mail->send()) {

} else {

}
            
                






								 echo "
				 <script>alert('Order Successful. Thank you for Shopping at SHU Book Store, You can track your order')</script>
				 <script>window.open('my_account.php?my_orders','_self')</script>";	
		 		
								  
								  
								  
							  }}

							  

						?>

</body>
</html>