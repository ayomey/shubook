-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 23, 2015 at 07:32 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `book_store`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `p_id` int(10) NOT NULL,
  `ip_add` varchar(255) NOT NULL,
  `qty` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `cat_id` int(100) NOT NULL,
  `cat_title` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_title`) VALUES
(1, 'Agriculture'),
(2, 'Biology'),
(3, 'Business'),
(4, 'Chemistry'),
(5, 'Computer science'),
(6, 'Economics'),
(7, 'Education'),
(8, 'History');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `com_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `pro_id` int(100) NOT NULL,
  `comment` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`com_id`, `user_id`, `pro_id`, `comment`, `date`) VALUES
(3, 8, 0, 'Perfect book ', '2015-07-19 14:30:35'),
(5, 8, 0, 'nice book', '2015-07-20 08:40:03'),
(6, 16, 41, 'Great Deal', '2015-07-23 16:19:31');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(100) NOT NULL,
  `p_id` int(100) NOT NULL,
  `u_id` int(100) NOT NULL,
  `s_id` int(100) NOT NULL,
  `qty` int(100) NOT NULL,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `p_id`, `u_id`, `s_id`, `qty`, `order_date`, `status`) VALUES
(13, 6, 8, 1, 1, '2015-07-21 16:33:41', 'in Progress'),
(14, 1, 13, 13, 1, '2015-07-23 03:26:56', 'in Progress'),
(15, 41, 16, 16, 1, '2015-07-23 16:19:03', 'in Progress'),
(16, 0, 16, 16, 1, '2015-07-23 16:19:11', 'in Progress'),
(17, 0, 16, 16, 1, '2015-07-23 16:19:14', 'in Progress'),
(18, 0, 16, 16, 1, '2015-07-23 16:27:33', 'in Progress');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int(100) NOT NULL,
  `product_cat` int(100) NOT NULL,
  `product_con` int(100) NOT NULL,
  `product_title` varchar(255) NOT NULL,
  `product_autor` varchar(200) NOT NULL,
  `product_price` int(100) NOT NULL,
  `product_desc` text NOT NULL,
  `product_image` text NOT NULL,
  `product_yearpub` int(100) NOT NULL,
  `product_keyword` text NOT NULL,
  `user_id` int(50) NOT NULL,
  `product_qty` int(100) NOT NULL,
  `product_qsold` int(100) NOT NULL,
  `condition` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_cat`, `product_con`, `product_title`, `product_autor`, `product_price`, `product_desc`, `product_image`, `product_yearpub`, `product_keyword`, `user_id`, `product_qty`, `product_qsold`, `condition`) VALUES
(1, 5, 0, 'Java', 'walter savitch', 200, 'Prefect book, it should be read!!', 'book1.jpg', 2009, 'Java, object oriented language', 1, 9, 3, 'New'),
(2, 5, 0, 'C++', 'Kqthy T', 45, '<p>great book</p>', 'C++.jpg', 2010, 'C++, oriented object language', 1, 5, 5, 'Used'),
(3, 5, 0, 'PHP', 'Jonhy', 56, '<p>Nice and useful one!</p>', 'cat.gif', 2012, 'PHP', 1, 3, 2, 'New'),
(4, 5, 0, 'My SQL', 'Paul', 56, 'perfect book', 'mysql.jpg', 2010, 'Mysql', 1, 7, 7, 'Used'),
(6, 5, 0, 'Cloud', 'Brain J.s', 56, '<p>Great Book for children!!</p>', 'download.jpg', 2010, 'cloud computing', 1, 9, 9, 'New'),
(8, 5, 0, 'JQuery', 'David', 30, '<p>Nice book</p>', 'lrg.jpg', 2010, 'JQuery', 1, 1, 5, 'New'),
(9, 1, 0, 'Agric', 'Maths', 15, 'Agricculture Books', 'agri-by Maths.jpg', 1987, 'Agric', 10, 30, 20, 'New'),
(10, 1, 0, 'Agriculture', 'Neil Sullivan', 20, 'Get Started with basic Agriculture ', 'Agricture by Neil Sullivan.jpg', 2001, 'Agric', 10, 5, 15, 'New'),
(11, 1, 0, 'Agric Drainage', 'Hiteshkumar Parmar', 21, 'Drainage book ', 'Agricultural Drainage Engineering by Hiteshkumar Parmar.jpg', 2002, 'Agric', 10, 15, 12, 'New'),
(12, 1, 0, 'Entomology', 'David Alford', 52, 'Detailed Entomology book for Agriculture', 'Agriculture Entomology by David Alford.jpg', 2014, 'Agric', 10, 40, 33, 'New'),
(13, 1, 0, 'Knowledge', 'Chavan', 70, 'Agric knowledge based book', 'Agriculture Knowledge by Chavan.jpg', 2015, 'Agric', 10, 10, 5, 'New'),
(14, 1, 0, 'Concept', 'Gloria Wee', 12, 'Concept of Agric made simple in this book', 'agriculture-concept-by Gloria Wee.jpg', 2004, 'Agric', 10, 30, 15, 'New'),
(15, 1, 0, 'Agricultural', 'Gupta', 17, 'Agricultural bank book ', 'Bank Agricultureal by Gupta.jpeg', 2007, 'Agric', 10, 5, 10, 'Used'),
(16, 1, 0, ' Agriculture', 'Sharma', 26, 'Basic Agric by Sharma', 'Basics of Agriculture by Sharma.jpg', 2006, 'agric', 10, 10, 20, 'New'),
(17, 1, 0, 'Grass based', 'gray tommy', 81, 'all about grass based agriculture', 'grass-based-dairy-farming by gray tommy.jpg', 2008, 'agric', 10, 20, 27, 'New'),
(18, 1, 0, 'Agriculte', 'Kantwa', 5, 'Basic objective agric book  by Kantwa', 'Objectibe Agriculture by Kantwa.jpg', 2005, 'agric', 10, 6, 4, 'Used'),
(19, 1, 0, 'Technology', 'PS Ahuja', 95, 'Tea science book  by PS Ahuja', 'Science of Tea Technology by PS Ahuja.jpg', 2009, 'Agric', 10, 10, 15, 'New'),
(20, 1, 0, 'You-Can-Farm ', 'Joel Salatin', 10, 'You-Can- farm in details', 'You-Can-Farm by Joel Salatin.jpg', 2010, 'agric', 10, 7, 3, 'Used'),
(21, 5, 0, 'Programing ', 'John Mitchell', 21, 'Basic concept of programming', 'Concepts of Programing by John Mitchell.jpg', 2005, 'Basic', 11, 6, 7, 'Used'),
(22, 5, 0, 'Domain', 'Martin Fowler', 20, 'Programming', 'Domain Languages by Martin Fowler.jpg', 2010, 'programming', 11, 6, 8, 'New'),
(23, 5, 0, 'Programing', 'David Watt', 11, 'Amazing book should you read to grow your Knowledge.', 'Programing Lang by David Watt.jpg', 2011, 'programming', 11, 8, 5, 'Used'),
(24, 2, 0, 'Advanced', 'John Turano', 51, 'Detailed Advance Bio', 'Advanced-Biology-by John Turano.jpg', 2005, 'Bio', 11, 13, 22, 'New'),
(25, 2, 0, 'Biology', 'Clarance Jee', 10, ' Clarance Jee book is interesting. should you read, the deliverable time will be 3 days after your order. ', 'a-textbook-of-biology- by Clarance Jee.jpeg', 1999, 'Bio', 11, 6, 3, 'Used'),
(36, 2, 0, 'Biology', 'Miller levine', 20, '<p>Great value</p>', 'Biology by Miller levine.jpg', 2005, 'Bio', 11, 14, 0, ''),
(37, 2, 0, 'Biology', 'Wyler', 5, '<p>Biology Book in details</p>', 'biology by Wyler.jpg', 2002, 'Bio', 11, 98, 0, ''),
(38, 2, 0, 'Biology', 'Jones James', 15, '', 'biology-book by Jones James.jpg', 1987, 'Bio', 11, 20, 0, ''),
(39, 2, 0, 'Geo', 'Geo', 15, '<p>NetGeo wide book on Biology</p>', 'book by Geo.jpg', 2010, 'Bio', 11, 10, 0, ''),
(40, 2, 0, 'Biology', 'Vinay Kumar', 5, '', 'Complete Biology by Vinay Kumar.jpeg', 2010, 'Bio', 11, 50, 0, ''),
(41, 2, 0, 'Essential', 'Nandi', 20, '', 'Essential_Biology by Nandi.jpg', 2002, 'Bio', 11, 13, 1, ''),
(43, 3, 0, 'Best Business', 'John Maxwell', 52, '<p>Best Business Book around</p>', 'best-business-books by John Maxwell.jpg', 2015, 'bus', 10, 54, 0, ''),
(44, 3, 0, 'Business', 'Stephen Covey', 15, '', 'best-business-by Stephen Covey.jpg', 2005, 'busi', 10, 14, 0, ''),
(45, 3, 0, 'Business', 'Peter Eeles', 21, '', 'building business objects by Peter Eeles.jpg', 2015, 'busi', 10, 20, 0, ''),
(46, 3, 0, 'Business Book', 'Jep Covey', 15, '', 'Business Book by Jep Covey.jpg', 2005, 'bus', 10, 10, 0, ''),
(47, 0, 0, 'Business Book', 'Napoletano', 5, '', 'Business-Books byNapoletano.jpg', 1987, 'busiss', 10, 14, 0, ''),
(48, 3, 0, 'Business', 'Jim Collins', 5, '', 'Good-to-Great-one-of-the-best-business-books-of-all-time by Jim Collins.jpg', 2010, 'busi', 10, 25, 0, ''),
(49, 3, 0, 'Business', 'Jim Collins', 52, '', 'GreatByChoice by Jim Collins.jpg', 2009, 'bus', 10, 50, 0, ''),
(50, 3, 0, 'Marketing', 'Seth Godin', 15, '', 'permission-marketing by Seth Godin.jpg', 1987, 'Agric', 10, 14, 0, ''),
(51, 3, 0, 'Heroes', 'William Cohen', 15, '', 'The Stuff of heroes by William Cohen.jpg', 2005, '', 10, 142, 0, ''),
(52, 4, 0, 'Chemisty', 'V Ahluwalia', 52, '', 'Basic Chemisty by V Ahluwalia.jpg', 2009, '', 0, 41, 0, ''),
(53, 4, 0, 'Chemisty', 'Alan Jones', 5, '', 'Chemistry by Alan Jones.jpg', 2010, '', 0, 14, 0, ''),
(54, 4, 0, 'Chemisty', 'David Chand', 20, '', 'chemistry- by David Chand.jpeg', 2002, '', 0, 25, 0, ''),
(55, 4, 0, 'Chemistry', 'Neil woods', 52, '', 'Chemistry by Neil woods.jpg', 2010, 'che', 0, 14, 0, ''),
(56, 4, 0, 'Chemisty', 'Rakesh Parashar', 5, '', 'Chemistry-by Rakesh Parashar.jpg', 2002, '', 0, 10, 0, ''),
(57, 4, 0, 'Chemisty', ' Dr Jauhar', 15, '', 'Chemistry-of-class-12th-by Dr Jauhar.jpg', 2009, '', 0, 14, 0, ''),
(58, 4, 0, 'Chemisty', 'Joery Lahann', 20, '', 'clickbook by Joery Lahann.jpg', 2010, '', 0, 14, 0, ''),
(59, 4, 0, 'Organic', 'Robert Hoffman', 20, '', 'hoffman_organic-chemistry by Robert Hoffman.jpg', 2005, '', 0, 14, 0, ''),
(60, 4, 0, 'Inorganic', 'Dr. O.p Yandon', 5, '', 'Inorganic Chemistry by Dr. O.p Yandon.jpg', 2010, 'che', 0, 14, 0, ''),
(61, 4, 0, 'Chemistry', 'Chand', 52, '', 'isc-chemistry- by Chand.jpeg', 2009, '', 0, 10, 0, ''),
(62, 6, 0, 'Eco Book', 'Orlando Wicker', 5, '<p>Eco Book.. nice book</p>', 'Eco Book by Orlando Wicker.jpg', 1987, 'eco', 0, 54, 0, ''),
(63, 6, 0, 'Economies', 'Alain Anderton', 20, '<p>Detailed</p>', 'Economices by Alain Anderton.jpg', 1987, 'eco', 15, 50, 0, ''),
(64, 6, 0, 'Economies', 'Mark Skousen', 15, '', 'Economic-Logic by Mark Skousen.jpg', 2005, 'eco', 15, 14, 0, ''),
(65, 6, 0, 'Economies', 'Michael A. Leeds', 21, '<p>Nice</p>', 'Economics by Michael A. Leeds.jpg', 2010, 'eco', 15, 25, 0, ''),
(66, 6, 0, 'One Lesson', 'Henry Hazlitt', 59, '<p>Great deal</p>', 'Economics in one lesson by Henry Hazlitt.jpg', 2015, 'eco', 15, 23, 0, ''),
(67, 6, 0, 'Economies', 'M. Juingan', 5, '', 'Economics Planning by M. Juingan.jpg', 1987, 'eco', 15, 20, 0, ''),
(68, 6, 0, 'Managerial', 'Prof. B.J Lathi', 15, '', 'managerial Economics by Prof. B.J Lathi.jpg', 2009, 'eco', 15, 25, 0, ''),
(69, 6, 0, 'Save Money', 'Stave Hardy', 21, '', 'Save Money by Stave Hardy.jpg', 2010, 'eco', 15, 50, 0, ''),
(70, 6, 0, 'Economies', 'Raymond Makewell', 5, '', 'Science-of-Economics- by Raymond Makewell.jpg', 2005, 'eco', 15, 20, 0, ''),
(71, 6, 0, 'Economies', 'David Driesen', 10, '', 'The Economic Dynamics of law By David Driesen.jpg', 2015, 'eco', 15, 54, 0, ''),
(72, 6, 0, 'Eco Book', 'Morgan Wilt', 21, '', 'the Economist by Morgan Wilt.jpg', 2010, 'eco', 15, 234, 0, ''),
(73, 6, 0, 'What is wrong ', 'Edward Fullbrook', 21, '', 'What is wrong with Economics by Edward Fullbrook.jpg', 2009, 'eco', 15, 25, 0, ''),
(74, 7, 0, 'The Book', 'Him Him', 5, '', 'crackingthebooks_masthead2 by Him Him.jpg', 1987, 'edu', 0, 10, 0, ''),
(75, 7, 0, 'Education', 'Bill Fitzerald', 15, '', 'drupal-for-education-and-e-learning by Bill Fitzerald.jpg', 2005, 'edu', 0, 20, 0, ''),
(76, 7, 0, 'Education', 'Lee Tim', 15, '', 'Education by Lee Tim.jpg', 2009, 'edu', 0, 88, 0, ''),
(77, 7, 0, 'Education', 'Wee Lim', 21, '', 'educational-books-by Wee Lim.jpg', 2015, 'edu', 0, 21, 0, ''),
(78, 7, 0, 'Education', 'James So', 5, '', 'education-books-high-school-by James So.jpg', 2005, 'edu', 0, 50, 0, ''),
(79, 7, 0, 'Education', 'Hallam', 21, '', 'education-study-books-high-school-university- by Hallam.jpg', 2015, 'edu', 0, 50, 0, ''),
(80, 7, 0, 'Education', 'John Dewey', 5, '', 'Experience&Education by John Dewey.jpg', 2010, 'eco', 0, 25, 0, ''),
(81, 7, 0, 'Education', 'Creak Blessing', 52, '', 'Kids Educational Books By Creak Blessing.jpg', 2009, 'edu', 0, 20, 0, ''),
(82, 7, 0, 'Education', 'Mary Junoa', 5, '', 'WVeducation By Mary Junoa.jpg', 1987, 'edu', 0, 50, 0, ''),
(83, 8, 0, 'World History', 'Gray Tom', 5, '', '5 General World History Books by Gray Tom.jpg', 2009, 'his', 11, 14, 0, ''),
(84, 8, 0, 'Ancient World', 'Susan Wise Bauer', 20, '', 'Ancient World by Susan Wise Bauer.jpg', 2009, 'his', 11, 54, 0, ''),
(85, 8, 0, 'history', 'Geo', 15, '', 'histor book by Geo.jpg', 1987, 'his', 11, 50, 0, ''),
(86, 8, 0, 'History Cover', 'Adam Hart', 15, '', 'history cover vyAdam Hart.jpg', 2009, 'his', 11, 25, 0, ''),
(87, 8, 0, 'History', 'Mahindra Huma', 52, '', 'History of the Book  by Mahindra Huma.jpg', 2015, 'his', 11, 14, 0, ''),
(88, 8, 0, 'History', 'Machu Picchu', 15, '', 'History-Books by Machu Picchu.jpg', 2010, 'his', 11, 50, 0, ''),
(89, 8, 0, 'History', 'Mill Wall', 5, '', 'history-books by Mill Wall.jpg', 2009, 'his', 11, 25, 0, ''),
(90, 8, 0, 'History', ' Doris Kearns', 20, '', 'Team of Rivals by Doris Kearns.jpg', 2005, 'his', 11, 54, 0, ''),
(91, 8, 0, 'History', 'Amos Webber', 15, '', 'We All Got history by Amos Webber.jpg', 2010, 'his', 11, 25, 0, ''),
(92, 8, 0, 'History', 'John Withington', 20, '', 'world-disaster by John Withington.jpg', 2010, 'his', 11, 25, 0, ''),
(93, 8, 0, 'History', 'McDougal Littell', 21, '', 'worldhistory by McDougal Littell.jpg', 2005, 'his', 11, 50, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE IF NOT EXISTS `ratings` (
  `rate_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `seller_id` int(100) NOT NULL,
  `pro_id` int(100) NOT NULL,
  `rate` tinyint(1) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`rate_id`, `user_id`, `seller_id`, `pro_id`, `rate`, `date`) VALUES
(17, 8, 1, 1, 1, '2015-07-21 16:12:30'),
(18, 8, 1, 8, 1, '2015-07-21 16:12:30'),
(19, 8, 1, 1, 1, '2015-07-21 16:14:28'),
(20, 16, 11, 41, 5, '2015-07-23 16:19:22');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(50) NOT NULL,
  `user_type` int(50) NOT NULL,
  `user_name` varchar(150) NOT NULL,
  `user_pass` varchar(150) NOT NULL,
  `user_email` varchar(150) NOT NULL,
  `user_address` varchar(150) NOT NULL,
  `user_postcode` varchar(150) NOT NULL,
  `user_country` varchar(150) NOT NULL,
  `user_ip` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_type`, `user_name`, `user_pass`, `user_email`, `user_address`, `user_postcode`, `user_country`, `user_ip`) VALUES
(1, 0, 'Intisar Omar', '123 ', 'mentesaromar@gmail.com', ' Glover Road', 'S8 0ZW', 'United Kingdom', '::1'),
(6, 1, 'Abdo', '345 ', 'Abd.omar@gmail.com', ' 53 Glover Road', 'S8 0ZW', 'United Kingdom', '::1'),
(8, 1, 'Arwa Omar', '123', 'Arwa.omar@gmail.com', ' Glover Road', 'S8 0ZW', 'United Kingdom', '::1'),
(9, 0, 'May', '123 ', 'may@yahoo.com', ' May street', 'M1 2pf', 'United Kingdom', '::1'),
(10, 0, 'James', '123 ', 'james@yahoo.com', ' sheffield', 's1 2pf', 'United Kingdom', '::1'),
(11, 0, 'Joe', '123 ', 'joe@yahoo.com', ' 101 sheffield', 's2 2pf', 'United Kingdom', '::1'),
(12, 1, 'Taofiqiya Adelakun', 'pass ', 'ayomey@gmail.com', ' 13 Rushby street', 'S4 8GN', 'United Kingdom', '::1'),
(13, 0, 'Barakat Raji', 'pass ', 'ayamgood30@gmail.com', ' 13 Rushby street', 'S4 8GN', 'United Kingdom', '::1'),
(14, 0, 'paul', '123 ', 'paul@yahoo.com', ' sheffield 101', 's1 2pf', 'United Kingdom', '::1'),
(15, 0, 'Gladys', '123 ', 'glad@yahoo.com', ' May street 101', 's1 5pp', 'United Kingdom', '::1'),
(16, 0, 'Chrisose', '123 ', 'chrisose29@hotmail.com', ' 101 sheffield', 's2 2pf', 'United Kingdom', '::1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`com_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`rate_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `com_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=94;
--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `rate_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
