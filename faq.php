<!DOCTYPE html>
<?php include("includes/c_header.php");?>

  <div class="container">
	<h2 class='title text-center'>Search <strong>Result</strong></h2>
	
	<div class="main">
		<div class="accordion">
			<div class="accordion-section">
				<a class="accordion-section-title" href="#accordion-1">What type of Account do i need to Buy and sell?</a>
				<div id="accordion-1" class="accordion-section-content">
					<p>We have two types of account</br> <strong>Buyer / Seller Account:</strong> This account is for student that are interested in selling and buy as well </br> <strong>Buyer Account:</strong> This account is for students who are interested in just buying some books</p>
				</div><!--end .accordion-section-content-->
			</div><!--end .accordion-section-->

			<div class="accordion-section">
				<a class="accordion-section-title" href="#accordion-2">What if i need to report a seller / Buyer?</a>
				<div id="accordion-2" class="accordion-section-content">
					<p>We take our customers very seriously and if you need to complain about anything leave us a message <a href "contact_us.php"> here</a> and we will try to sort you out as soon as possible.</p>
			</div><!--end .accordion-section-->

			<div class="accordion-section">
				<a class="accordion-section-title" href="#accordion-3">What if i need a book and i cant find it?</a>
				<div id="accordion-3" class="accordion-section-content">
					<p>We take our customers enquiry very seriously and if you can leave us a message <a href "contact_us.php"> here</a> and we will try to sort you out as soon as possible.</p>
				</div><!--end .accordion-section-content-->
			</div><!--end .accordion-section-->
		</div><!--end .accordion-->
	</div></div>
</body>
</html>